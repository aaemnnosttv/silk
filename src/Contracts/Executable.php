<?php

namespace Silk\Contracts;

interface Executable
{
    /**
     * @return void
     */
    public function execute();
}
