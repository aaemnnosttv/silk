# Silk

[![Travis Build](https://img.shields.io/travis/aaemnnosttv/silk/master.svg)](https://travis-ci.org/aaemnnosttv/silk)
[![Coveralls](https://img.shields.io/coveralls/aaemnnosttv/silk/master.svg)](https://coveralls.io/github/aaemnnosttv/silk?branch=master)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/g/aaemnnosttv/silk/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/g/aaemnnosttv/silk/?branch=master)
[![HHVM (branch)](https://img.shields.io/hhvm/silk/silk/master.svg)](http://hhvm.h4cc.de/package/silk/silk)
[![Gitter](https://img.shields.io/gitter/room/aaemnnosttv/silk.svg)](https://gitter.im/aaemnnosttv/silk)
[![Packagist](https://img.shields.io/packagist/v/silk/silk.svg)](https://packagist.org/packages/silk/silk)


A modern API for WordPress.

**This project is still in early development and may introduce breaking changes.**

--

## What is Silk?

Silk is a library designed as a thin layer on top of WordPress, abstracting away the mess of functions into a clean and expressive object-oriented API.  Use as much or little as you want, while maintaining compatibility with everything else that's meant to work with WordPress.

[Read the documentation](https://github.com/aaemnnosttv/silk-docs) for examples and more.

Or check out the [API documentation](https://api.silk.aaemnnost.tv/master/) if you're into that kind of thing.

## Installation

#### Via Composer (Recommended)
```
composer require silk/silk:^0.10
```

## Contributing

Contributions are welcome! If you're interested in contributing to the project, please open an issue first.  I would hate to decline a Pull Request forged by hours of effort for any reason, so please read the contribution guidelines first.

#### Related Projects and Inspiration
- [Laravel](https://laravel.com)
- [Timber](https://github.com/jarednova/timber)
- [Corcel](https://github.com/jgrossi/corcel)
- [WordPlate](https://github.com/wordplate/wordplate)
- [WP Eloquent](https://github.com/tareq1988/wp-eloquent)
